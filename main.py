"""
The main class that calls everything. This is the starting point of the pipeline, as well as a playground to try things out.
"""
from random import randint
import gym
import gym_flappy_bird
import src.dataHandling as dataHandling
import src.Buffer as Buffer
import src.DQNLearningAgent as Agent
import numpy as np
import os


def human_play_and_log(steps):
    env = gym.make('flappy-bird-v0')
    # get ready to log:
    # sample initial state for log:
    framedata = []
    state = (279, -69, 227, -145, 245, 135, 1)
    reward = 0.1
    env.reset()
    done = False
    for steps in range(steps):
        if not done:
            inp = input()
            action = 1 if inp == "1" else 0
            state_prime, reward, done, info = env.step(action)
            framedata.append((state, action, reward, state_prime))
            state = state_prime
            env.render()
        else:
            # go to start state
            state = (279, -69, 227, -145, 245, 135, 1)
            env.reset()
            done = False
    dataHandling.log_frame_data(framedata)
    env.close()
    quit()


def dqn_play_and_log(games, path_to_demonstration_buffer, training=True):
    env = gym.make('flappy-bird-v0')

    if not training:
        # slow it down so you can observe
        env.lock_fps(30)

    buffer_path = path_to_demonstration_buffer
    # if you enter a valid path here it will load the buffer automatically in the following
    if os.path.exists(buffer_path):
        amount_of_demonstrations = dataHandling.get_amount_of_demonstrations(
            buffer_path)
        print("Loading Demonstration Buffer...")
    else:
        amount_of_demonstrations = 0
        print("No Demonstration Data is being used\n")

    # counter for logging progress:
    gamedata = []
    frame_counter = 0
    game_counter = 0
    highscore = 0
    bad_games_in_a_row = 0
    weights_loaded = ""
    # sample initial state for log:
    state = (279, -69, 227, -145, 245, 135, 1)
    reward = 0.1

    state_size = 7
    action_size = env.action_space.n  # 2
    agent = Agent.DQNLearningAgent(
        state_size, action_size, amount_of_demonstrations)
    batch_size = 32
    if os.path.exists(buffer_path):
        agent.memory.log2Buffer(buffer_path)
        print("Demonstration Buffer loaded.\n")

    # playing
    for episode in range(games):

        game_counter += 1
        score = 0
        state = env.reset()
        state = np.reshape(state, [1, state_size])
        done = False
        if not training:
            env.lock_fps(30)

        while not done:
            action = agent.act(state)
            state_prime, reward, done, _ = env.step(action)
            if not training:
                env.render()

            state_prime = np.reshape(state_prime, [1, state_size])
            agent.remember(state, action, reward, state_prime,
                           done, origin="exploration")
            state = state_prime
            score = env.score
            frame_counter += 1

        # after each game, do:
        # if score !=0:
        if agent.memory.size() > batch_size:
            agent.replay(batch_size)

        print("game: {}/{}, epsilon: {}, score: {}, highscore: {}"
              .format(episode, games, str(agent.epsilon)[:5], score, highscore))
        # logging stuff:
        gamedata.append((game_counter, score, frame_counter, highscore))
        frame_counter = 0
        state = (279, -69, 227, -145, 245, 135, 1)

        # back to logic:
        agent.update_stable_model()

        # saving good weights after we wrote it to stable:
        if score > highscore:
            highscore = score
            if highscore >= 5:
                agent.save("weights/" + "DQN_" + str(episode) +
                           "_episodes_"+str(highscore)+"_highscore_weights.h5")

        # if we were once good but then diverged, reset to that state!
        if highscore >= 5:
            if score == 0 or score == 1:
                bad_games_in_a_row += 1
                if bad_games_in_a_row >= 15:
                    # reset bad_game counter and weights
                    bad_games_in_a_row = 0
                    weights = dataHandling.get_newest_weights("weights/")
                    if weights != weights_loaded:
                        agent.load(weights)
                        print(
                            "i'm sorry that I forgot how to play the game again.\nI resetted to my good boy behaviour again! ")
                        print("now using:", weights)
                        weights_loaded = weights
                    else:
                        print("not reloading to get out of reload-loop")

        if episode > 200 and highscore < 2:
            print("Not a promising run, stopping this one")
            return
        # if episode > 600 and highscore < 5:
        #    print("not a promising run, stopping this one")

    # done playing
    savedDataFilePath = dataHandling.log_game_data(gamedata)
    try:
        dataHandling.get_game_stats(savedDataFilePath)
    except FileNotFoundError:
        print("not giving game stats because you didn't log")

    inp = input("do you want to safe the final weights? [Y]/n")
    if inp != "n":
        agent.save("weights/" + "DQN_" + str(episode) +
                   "_episodes_"+str(highscore)+"_highscore_weights.h5")


def show_off(path_to_weights_to_load):
    """
    Load a pretrained model and have it play the game indefinitely. No more
    learning or exploration happens here, this just uses the policy that is
    saved in the form of weights as a file. When the bird dies, it will restart.
    Just playing forever with the current policy.
    :param path_to_weights_to_load: path to the file containing the weights
    :return: None
    """
    env = gym.make('flappy-bird-v0')

    state_size = 7
    action_size = env.action_space.n
    agent = Agent.DQNLearningAgent(state_size, action_size, 0)
    agent.load(path_to_weights_to_load)
    agent.epsilon = 0
    done = False
    while True:
        state = env.reset()
        state = np.reshape(state, [1, state_size])
        # env.lock_fps(30)
        while not done:  # this sounds so intuitive, wow
            action = agent.act(state)
            next_state, reward, done, _ = env.step(action)
            env.render()
            next_state = np.reshape(next_state, [1, state_size])
            state = next_state
        else:
            done = False


def find_good_models():
    while True:
        dqn_play_and_log(5000, "logs/frames_combined.csv", training=True)


if __name__ == '__main__':

    show_off("weights/experience/DQN_3000_episodes_1226_highscore_weights.h5")
    # human_play_and_log(3000)
    #dqn_play_and_log(3000, "", training=False)
    # find_good_models()

    # weights = dataHandling.get_newest_weights("weights/")
    # print(weights)
