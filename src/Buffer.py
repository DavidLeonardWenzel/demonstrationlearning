import random
import src.dataHandling as dataHandling
import numpy as np


class Buffer:
    """
    A buffer object to handle experiences and demonstrations

    IMPORTANT: REWARDS HAVE TO BE NORMALISED 0-1
    """

    def __init__(self, max_experience_capacity, amount_of_demonstrations, states=list(), actions=list(), rewards=list(),
                 followup_states=list(), dones=list(), sources=list()):
        """
        create buffer
        """
        self.amount_of_demonstrations = amount_of_demonstrations
        self.max_capacity = max_experience_capacity + amount_of_demonstrations
        self.states = states
        self.actions = actions
        self.rewards = rewards
        self.followup_states = followup_states
        self.dones = dones
        self.sources = sources

    def remove_oldest_self_generated_transition(self):
        """
        remove the oldest transition that is not a demonstration
        """
        self.remove(self.amount_of_demonstrations)

    def sample(self, n):
        """
        get a random portion of the buffer
        """
        batch = list()
        for _ in range(n):
            i = random.choice(range(self.size()))
            while self.actions[i] == 0 and (random.uniform(0, 1) > .6):
                # skew sampling probability towards samples where the bird flaps cause those are more interesting
                i = random.choice(range(self.size()))
            batch.append((self.states[i], self.actions[i], self.rewards[i], self.followup_states[i], self.dones[i],
                          self.sources[i]))
        return batch

    def copy(self):
        """
        returns a deep copy of the buffer
        """
        return Buffer(self.max_capacity.copy(), self.states.copy(), self.actions.copy(), self.rewards.copy(),
                      self.followup_states.copy(), self.sources.copy())

    def store(self, state, action, reward, followup_state, done, source):
        """
        put a new sample in the buffer
        source is a string either "demonstration" or "experience"
        """
        # every time we store something in the buffer, we might need to make space first
        if self.size() + 1 > self.max_capacity:
            self.remove_oldest_self_generated_transition()
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)
        self.followup_states.append(followup_state)
        self.dones.append(done)
        self.sources.append(source)

    def remove(self, index):
        """
        remove item at index from buffer
        """
        self.states.pop(index)
        self.actions.pop(index)
        self.rewards.pop(index)
        self.followup_states.pop(index)
        self.dones.pop(index)
        self.sources.pop(index)

    def size(self):
        """
        returns the amount of samples in the buffer
        """
        return len(self.states)

    def log2Buffer(self, path):
        """takes the raw logdata and outputs a tuple containing 
        1. ) a nested numpy array with one feature-array per row
        2. ) a numpy array with the correct classifications
        """
        logData = dataHandling.read(path)
        for row in logData:
            rowSplit = row.split(", ")

            state = np.array([int(value) for value in
                              rowSplit[0:7]])
            state_bar = np.array([int(value) for value in rowSplit[9:]])

            self.states.append(state)
            self.actions.append(int(rowSplit[7]))
            self.rewards.append(float(rowSplit[8]))
            self.followup_states.append(state_bar)
            self.dones.append(
                "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")  # <----- This one is new because apparently we need it. Needs to be logged and read as well now
            self.sources.append("demonstration")  # this was missing, added it so the overflow doesn't delete these
