"""
This module is meant to handle all incoming and outgoing data
"""

from datetime import datetime
import src.Buffer as Buffer
import os

def log_frame_data(framedata):
    if input("Do you want to safe this playthroughs frame data (every state)? [Y]/n\n") != "n":

        time = datetime.now()
        timestamp = str(time.month) + "." + str(time.day) + "_" + str(time.hour) + "." + str(time.minute)

        my_f = open("logs/" + timestamp + "_FRAMES.csv", "w", encoding="utf-8")

        my_f.write(
            "x dist to pipes right corner, y dist to lower pipe, x dist to pipes left corner, y dist to upper pipe,  dist to ceiling, dist to ground, y velocity, action, reward, x dist to pipes right corner prime, y dist to lower pipe prime, x dist to pipes left corner prime, y dist to upper pipe prime,  dist to ceiling prime, dist to ground prime, y velocity prime\n")
        for frame in framedata:
            state, action, reward, state_bar = frame

            x_dist_lower_right, y_dist_lower_pipe, x_dist_lower_left, y_dist_upper_pipe, dist_ground, dist_ceiling, velocity = state
            x_dist_lower_right_prime, y_dist_lower_pipe_prime, x_dist_lower_left_prime, y_dist_upper_pipe_prime, dist_ground_prime, dist_ceiling_prime, velocity_prime = state_bar

            my_f.write(
                str(x_dist_lower_right) + ", " + str(y_dist_lower_pipe) + ", " + str(x_dist_lower_left) + ", " + str(
                    y_dist_upper_pipe) +", " + str(dist_ground) + ", " + str(dist_ceiling) + ", " + str(velocity) +

                ", " + str(action) + ", " + str(reward) + ", " +
                # next states data:
                str(x_dist_lower_right_prime) + ", " + str(y_dist_lower_pipe_prime) + ", " + str(
                    x_dist_lower_left_prime) + ", " + str(y_dist_upper_pipe_prime) + ", " + str(dist_ground_prime) + ", " + str(dist_ceiling_prime) + ", " + str(velocity_prime) +
                "\n")

        my_f.close()


def log_game_data(gamedata):
    """
    logs overall statistics like frames played, games played, highscore
    """
    time = datetime.now()
    timestamp = str(time.month) + "." + str(time.day) + "_" + str(time.hour) + "." + str(time.minute)

    if input("Do you want to safe this playthroughs game data (scores, highscores)? [Y]/n\n") != "n":

        my_f = open("logs/" + timestamp + "_GAMES.csv", "w", encoding="utf-8")

        my_f.write("game_counter, score, frames, highscore\n")
        for game in gamedata:
            game_counter, score, frame_counter, highscore = game
            my_f.write(str(game_counter) + ", " + str(score) + ", " + str(frame_counter) + ", " +str(highscore) +"\n")
        print("saved!")
        
    return "logs/" + timestamp + "_GAMES.csv" 


def get_game_stats(path):
    """
    expects a path to a file written by log_game_data()
    returns a 3 tuple containing 1
        1) highest pipe counter of all games
        2) average pipes covered
        3) average frames covered
    2&3 will be in a ratio of ~1:37 unless you die alot
    """
    rawLog = read(path)
    pipe_ttl = 0
    frame_ttl = 0
    game_ttl = 0
    for game_played in rawLog:
        # unpack and cast values:
        game_counter, pipes_covered, frames_travelled, highscore = game_played.split(", ")
        game_counter, pipes_covered, frames_travelled, highscore = int(game_counter), int(pipes_covered), int(frames_travelled), int(highscore)
        # count:
        game_ttl += 1
        pipe_ttl += pipes_covered
        frame_ttl += frames_travelled

    pipe_avg = pipe_ttl / game_ttl
    frame_avg = frame_ttl / game_ttl
    print("Max Pipe Score:\t\t", highscore)
    print("Avg. Pipe Score:\t", pipe_avg)
    print("Avg.  Frame Score:\t", frame_avg)
    return highscore, pipe_avg, frame_avg


def read(path):
    with open(path, 'r', encoding="utf-8") as f:
        return f.read().splitlines()[1:]  # the '1:'' excludes the header row


def get_amount_of_demonstrations(path):
    with open(path, 'r', encoding="utf-8") as f:
        return len(f.read().splitlines()[1:])  # the '1:'' excludes the header row


def get_newest_weights(path):
    files = os.listdir(path)
    paths = [os.path.join(path, basename) for basename in files]
    return max(paths, key=os.path.getctime)


"""

sample1[STATE, ACTION, REWARD, FOLLOWUP_STATE]
sample2[STATE, ACTION, REWARD, FOLLOWUP_STATE]
sample3[STATE, ACTION, REWARD, FOLLOWUP_STATE]
sample4[STATE, ACTION, REWARD, FOLLOWUP_STATE]


sample54["109y20distancetonextlowerpipe", "jump", 1, "208x,160y"]
sample57["109y20distancetonextlowerpipe", "pass", -100, "0x,0y"]

"""
