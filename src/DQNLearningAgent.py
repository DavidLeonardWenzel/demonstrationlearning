"""
This module is meant to hold the actual Q-Learning agent that we want to use
"""

from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import RMSprop
from keras.losses import mse
from keras import backend as K
import tensorflow as tf
import random
import numpy as np
from keras.utils import plot_model
import os
import src.Buffer as Buffer
import src.dataHandling as dataHandling


class DQNLearningAgent:
    """
    This class is our Agent that we want to train
    """

    def __init__(self, states, actions, amount_of_demonstrations):
        """
        builds a new agent
        """
        self.states = states  # the number of diff. environment observations, feature vec length
        self.actions = actions
        self.memory = Buffer.Buffer(max_experience_capacity=2000, amount_of_demonstrations=amount_of_demonstrations)
        # hyper params:
        self.gamma = 0.99  # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.99
        self.loss = mse  # change to huber or some combination of losses here
        self.opt = RMSprop(
            lr=0.005,
            rho=0.9,
            epsilon=None,
            # not to be confused with epsilon decay above, this is for the learning rate and not exploration rate
            decay=0.0)
        self.stable_model = self.assemble()  # avengers, assemble!
        # this is the stable policy that we follow
        self.development_model = self.assemble()  # part 2!
        # this is the development branch
        self.update_stable_model()  # copies weights from dev to stable

    def assemble(self):
        # Neural Net for Deep-Q learning 
        stable_model = Sequential()
        stable_model.add(Dense(7, input_dim=self.states, activation='relu', name="MainInput"))
        stable_model.add(Dense(7, activation='relu', name="FirstDense"))
        stable_model.add(Dense(4, activation='relu', name="SecondDense"))
        stable_model.add(Dense(4, activation='relu', name="ThirdDense"))
        stable_model.add(Dense(self.actions, activation='linear', name="BinaryOutput"))
        stable_model.compile(loss=self.loss, optimizer=self.opt)
        plot_model(stable_model, to_file='model.png')
        return stable_model

    def update_stable_model(self):
        # currently updating every episode, i.e., per death
        # copies weights from the development model to the stable one in use
        self.stable_model.set_weights(self.development_model.get_weights())

    def remember(self, state, action, reward, next_state, done, origin):
        self.memory.store(state, action, reward, next_state, done, origin)

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            choice = random.choices([0, 1], [0.83, 0.17])
            # print("randomly chose to do ", choice)
            return choice[0]
        act_values = self.stable_model.predict(state)
        return np.argmax(act_values[0])  # returns greedy action

    def replay(self, batch_size):
        # fitting dev model after each state
        minibatch = self.memory.sample(batch_size)
        for state, action, reward, next_state, done, _ in minibatch:
            target = self.get_target(state, next_state, action, reward, done)
            try:
                self.development_model.fit(state, target, epochs=1, verbose=0)
            except ValueError:
                state =self.transform_state(state)
                target = self.stable_model.predict(state)
        self.decay()

    def transform_state(self, state):
        return np.array([int(value) for value in state]).reshape(1,self.states)

    def get_target(self, state, next_state, action, reward, done):
        # q value is highest activation-value in last layer
        # calculates q values, then uses bellman approximation to get target/ max exp. value"""
        try:
            target = self.stable_model.predict(state)
        except ValueError:
            state =self.transform_state(state)
            next_state = self.transform_state(next_state)
            target = self.stable_model.predict(state)
        
        if done:
            target[0][action] = reward
        else:
            targets = self.development_model.predict(next_state)[0]
            target[0][action] = reward + self.gamma * np.amax(targets)
        return target

    def load(self, name):
        self.stable_model.load_weights(name)

    def load_dev(self, name):
        self.development_model.load_weights(name)

    def save(self, name):
        self.stable_model.save_weights(name)

    def decay(self):
        # """
        # here you can exchange the epsilon decay function. Good ideas might be a linear decay or a logarithmic decay
        # """
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    