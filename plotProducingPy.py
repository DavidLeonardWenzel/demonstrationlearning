#what is my purpose?
#You plot the figures!


#paths will probably have to be corrected!
import pandas as pd
import matplotlib as plt

if __name__ == '__main__':

	vanillaDQNData = pd.read_csv("demonstrationlearning/logs/vanillaDqn_3000.csv")
	expDQNData = pd.read_csv("demonstrationlearning/logs/exp_5000.csv")
	vanillaDQNData.columns = 'Episode', 'Score', 'Frames', 'VanillaDQNHighscore'
	expDQNData.columns = "ExpDQNEpisode", "Score", "Frames", "ExpDQNHighscore"
	expDQNData= expDQNData.truncate(after=2000)
	vanillaDQNData= vanillaDQNData.truncate(after=2000)
	
	fig2 = expDQNData.plot(x ="ExpDQNEpisode", y = "Score")
	fig3 = vanillaDQNData.plot(x ="Episode", y = "Score")

	data_joined = vanillaDQNData.join(expDQNData.ExpDQNHighscore)

	fig1 = data_joined.plot(x='Episode', y=["VanillaDQNHighscore", "ExpDQNHighscore"], kind='line')

	fig1.get_figure().savefig("plots/demonstrationlearning/highscore.eps",  format='eps')
	fig2.get_figure().savefig("plots/demonstrationlearning/ExpDiverge.eps",  format='eps')
	fig3.get_figure().savefig("plots/demonstrationlearning/VanillaDiverge.eps",  format='eps')